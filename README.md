# librist-rust

A [Rust](https://www.rust-lang.org/) wrapper for the
[librist](https://code.videolan.org/rist/librist) library.

There are two sub-projects here,

 - [**librist-rust**](librist-rust) contains the actual public Rust API that you
   want to use
 - [librist-sys](librist-sys) contains low-level, 'unsafe' wrappers around
   librist, created automatically using
   [bindgen](https://rust-lang.github.io/rust-bindgen/).  This is only used to
   implement the 'safe' API in librist-rust.
