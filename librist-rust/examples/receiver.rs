use librist_rust::{LoggingSettings, LogLevel, ReceiverContext, Profile, PeerConfig};
use std::io::stderr;

fn main() {
    let url = std::env::args().skip(1).next()
        .expect("Please supply one URL argument");
    let peer_config = PeerConfig::parse_address(&url)
        .expect(&format!("Unable to parse {:?}",url));
    let logging_settings = LoggingSettings::file(LogLevel::Info, stderr())
        .expect("LoggingSettings::file() failed");
    let mut ctx = ReceiverContext::create(Profile::Simple, logging_settings, |block| {
        println!("Got a data block ts={}", block.ts_ntp()>>32);
        hexdump::hexdump(block.payload());
    })
        .expect("Context::receiver_create failed");

    ctx.peer_create(peer_config)
        .expect("peer_create() failed");

    ctx.start()
        .expect("ctx.start() failed");
    std::thread::park();
}