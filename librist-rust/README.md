# librist-rust

[![crates.io version](https://img.shields.io/crates/v/librist-rust.svg)](https://crates.io/crates/librist-rust)
[![Documentation](https://docs.rs/librist-rust/badge.svg)](https://docs.rs/librist-rust)

Rust wapper for [librist](https://code.videolan.org/rist/librist), allowing you to use the [RIST](https://www.rist.tv/)
protocol within Rust applications.

## Feature support

This wrapper is in very early stages and currently does not support a number of librist features:

 - [ ] Sending - not supported
 - [x] Receiving - basic support
 - [ ] Custom authentication hooks - not supported
 - [ ] Out-of-band data - not supported
 - [x] librist logging configuration supported
 - [ ] stats reporting - not supported
 - [ ] data callback - not supported
 - [ ] NAK control - not supported
 
 ## Example
 
 Receive and hex-dump RIST packets,
 
 ```rust
use librist_rust::{LoggingSettings, LogLevel, ReceiverContext, Profile, PeerConfig, RistError, DataReadResponse};
use std::io::stderr;

fn main() {
    let url = std::env::args().skip(1).next()
        .expect("Please supply one URL argument");
    let peer_config = PeerConfig::parse_address(&url)
        .expect(&format!("Unable to parse {:?}",url));
    let logging_settings = LoggingSettings::file(LogLevel::Info, stderr())
        .expect("LoggingSettings::file() failed");
    let mut ctx = ReceiverContext::create(Profile::Main, logging_settings)
        .expect("Context::receiver_create failed");

    ctx.peer_create(peer_config)
        .expect("peer_create() failed");

    // Have to call these or the connection with the librist 'ristsender' tool will not be
    // established
    ctx.auth_handler_set().expect("auth_handler_set() failed");
    ctx.oob_callback_set().expect("oob_callback_set() failed");

    ctx.start();
    loop {
        match ctx.data_read(std::time::Duration::from_secs(1)) {
            Ok(DataReadResponse::NoData) => {
                println!("No data received within timeout window")
            },
            Ok(DataReadResponse::Data { block, queue_size }) => {
                println!("Got a data block; queue now at {} items", queue_size);
                hexdump::hexdump(block.payload())
            },
            Err(e) => {
                println!("data_read() failed {:?}", e);
                return;
            },
        }
    }
}
```

The above example is in this project, so from a checked-out copy you can run the following to dump payloads
of RIST packets sent to port 12344 on localhost,

```shell script
cargo run --release --example receiver rist://@127.0.0.1:12344
```

